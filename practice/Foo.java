import org.python.util.*;

public class Foo
{
 public static void main (String[] args)
 {
  try
  {
   PythonInterpreter.initialize(System.getProperties(), System.getProperties(), new String[0]);
   PythonInterpreter interp = new PythonInterpreter();
   interp.execfile("Building.py");
  }
  catch (Exception e)
  {
   e.printStackTrace();
  }
 }
}
