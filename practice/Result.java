package org.jython.book.interfaces;

public interface Result{
  public float getAdd();
  public float getSub();
  public float getMult();
  public float getDiv();
}
