package org.jython.book.util;

import org.jython.book.interfaces.Result;
import org.python.core.PyObject;
import org.python.core.PyFloat;
import org.python.util.PythonInterpreter;

public class ResultFactory{
  private PyObject resultClass;

  public ResultFactory(){
    PythonInterpreter interpreter = new PythonInterpreter();
    interpreter.exec("from calc import calc");
    resultClass = interpreter.get("calc");
  }

  public Result create (float answer){
    PyObject calcObject = resultClass.__call__(new PyFloat(answer));
    return (Result)calcObject.__tojava__(Result.class);
  }



}
